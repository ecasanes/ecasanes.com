<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_plugins_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'zP([$<~H{Z+V[w&ad;_EDIfP$12}^kWWxE^QUF=KUAvJQG*hDM;v!gW]?Q:jTO^Q');
define('SECURE_AUTH_KEY',  ':eF4j{+-x;.Zx}8 rhu;g[%]nNZu&.UVm}?hbff7jY PcXx-P,C,d)qrIX~5Dn7,');
define('LOGGED_IN_KEY',    'o.!.$)mv|:dhE%sT;z&9ikdpekR`D]Wi3A*p(L95qFuGN3ug229r+z PiZ8v( rK');
define('NONCE_KEY',        'D`<^bZLL|%uMxP,X;u0+=QQJQ@zHwgO|:(MrMBsY&}0-A=Ur0hR>zOMdn1p;sX9=');
define('AUTH_SALT',        '_p^i63E3i[nP!/jjc/S;ck*tN(O.3bHB(?a1h@@B`5N*lMm<eW.tQ;K.(]O0{4y#');
define('SECURE_AUTH_SALT', ',UZs3VjW_0H_97QK^2or%_g<i[d9-MY#S(uE|S6m(aT9GF|_s3j!?<CKRaUTzRGR');
define('LOGGED_IN_SALT',   'V4_H]1Be)tw?LwZ(T#@M$.vhljh43qQcR%t^Oh[rjE//rYmqXb/6RC 9?0BGutx-');
define('NONCE_SALT',       '8CL@Z*8<y3%FVkQvp!q}ktwdVfqA9IA(i?U*j)Wx#^pJE<T`I[^h?-)Q#dDe3hR6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
